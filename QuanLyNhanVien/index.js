const DSNV = "DSNV";
var dsnv = [];
// Json
function luuLocalStorage() {
  let jsonDsnv = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jsonDsnv);
}
var dataJson = localStorage.getItem(DSNV);
console.log("dataJson: ", dataJson);
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson);
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new nhanVien(
      item.tk,
      item.ten,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
  renderDsnv(dsnv);
}
function themNV() {
  var nv = layThongTinTuFormNv();
  var isValid = true;
  // validate tk
  isValid =
    kiemTraRongNv(nv.tk, "tbTKNV", "Tai khoan NV khong duoc de trong") &&
    kiemTraSoLuong(nv.tk, "tbTKNV", "Tai khoan NV phai tu 4-6 ky so");
  // validate ten NV
  isValid =
    kiemTraRongNv(nv.ten, "tbTen", "Ten NV khong duoc de trong") &&
    kiemTraChu(nv.ten, "tbTen", "Ten NV phai la chu");
  // validate email
  isValid =
    kiemTraRongNv(nv.email, "tbEmail", "Email khong duoc de trong") &&
    kiemTraEmail(nv.email, "tbEmail", "Email phai dung dinh dang");
  // validate mat khau
  isValid =
    kiemTraRongNv(nv.matKhau, "tbMatKhau", "Mat khau khong duoc de trong") &&
    kiemTraMatKhau(
      nv.matKhau,
      "tbMatKhau",
      "Mat khau từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  // validate ngay lam
  isValid =
    kiemTraRongNv(nv.ngayLam, "tbNgay", "Ngay lam khong duoc de trong") &&
    kiemTraNgay(
      nv.ngayLam,
      "tbNgay",
      "ngay lam phai theo dinh dang dd/mm/yyyy"
    );
  // validate luong co ban
  isValid =
    kiemTraRongNv(
      nv.luongCB,
      "tbLuongCB",
      "Luong co ban khong duoc de trong"
    ) &&
    kiemTraLuongCB(
      nv.luongCB,
      "tbLuongCB",
      "luong co ban phai tu 1000000 den 20000000"
    );
  // validate chuc vu

  isValid = kiemTraChucVu(
    nv.chucVu,
    "tbChucVu",
    "Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)"
  );
  // validate gio lam
  isValid =
    kiemTraRongNv(nv.gioLam, "tbGiolam", "Gio lam khong duoc de trong") &&
    kiemTraGioLam(nv.gioLam, "tbGiolam", "Gio lam phai tu 80 den 200 gio");
  if (isValid) {
    dsnv.push(nv);
    renderDsnv(dsnv);
    luuLocalStorage();
  }
}
function xoaNV(tk) {
  var viTri = timKiemViTri(tk, dsnv);
  console.log("viTri: ", viTri);
  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
    renderDsnv(dsnv);
    luuLocalStorage();
  }
}
function suaNV(tk) {
  var viTri = timKiemViTri(tk, dsnv);
  if (viTri == -1) return;
  var data = dsnv[viTri];
  showThongTinLenFormNV(data);
  document.getElementById("tknv").disabled = true;
}
function capNhat() {
  var data = layThongTinTuFormNv();
  var viTri = timKiemViTri(data.tk, dsnv);
  if (viTri == -1) return;
  var isValid = true;
  // validate tk
  isValid =
    kiemTraRongNv(data.tk, "tbTKNV", "Tai khoan NV khong duoc de trong") &&
    kiemTraSoLuong(data.tk, "tbTKNV", "Tai khoan NV phai tu 4-6 ky so");
  // validate ten NV
  isValid =
    kiemTraRongNv(data.ten, "tbTen", "Ten NV khong duoc de trong") &&
    kiemTraChu(data.ten, "tbTen", "Ten NV phai la chu");
  // validate email
  isValid =
    kiemTraRongNv(data.email, "tbEmail", "Email khong duoc de trong") &&
    kiemTraEmail(data.email, "tbEmail", "Email phai dung dinh dang");
  // validate mat khau
  isValid =
    kiemTraRongNv(data.matKhau, "tbMatKhau", "Mat khau khong duoc de trong") &&
    kiemTraMatKhau(
      data.matKhau,
      "tbMatKhau",
      "Mat khau từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  // validate ngay lam
  isValid =
    kiemTraRongNv(data.ngayLam, "tbNgay", "Ngay lam khong duoc de trong") &&
    kiemTraNgay(
      data.ngayLam,
      "tbNgay",
      "ngay lam phai theo dinh dang dd/mm/yyyy"
    );
  // validate luong co ban
  isValid =
    kiemTraRongNv(
      data.luongCB,
      "tbLuongCB",
      "Luong co ban khong duoc de trong"
    ) &&
    kiemTraLuongCB(
      data.luongCB,
      "tbLuongCB",
      "luong co ban phai tu 1000000 den 20000000"
    );
  // validate chuc vu

  isValid = kiemTraChucVu(
    data.chucVu,
    "tbChucVu",
    "Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)"
  );
  // validate gio lam
  isValid =
    kiemTraRongNv(data.gioLam, "tbGiolam", "Gio lam khong duoc de trong") &&
    kiemTraGioLam(data.gioLam, "tbGiolam", "Gio lam phai tu 80 den 200 gio");
  if (isValid) {
    dsnv[viTri] = data;
    renderDsnv(dsnv);
    luuLocalStorage();
    document.getElementById("tknv").disabled = false;
  }
}
function timNV() {
  let data = document.getElementById("searchName").value;
  let dsTimKiem = [];
  for (index = 0; index < dsnv.length; index++) {
    var currentNV = dsnv[index];
    if (currentNV.xepLoai() == data) {
      dsTimKiem.push(currentNV);
    }
  }
  renderDsnv(dsTimKiem);
}
