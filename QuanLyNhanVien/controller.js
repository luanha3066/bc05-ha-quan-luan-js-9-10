function layThongTinTuFormNv() {
  var taiKhoan = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;
  var nv = new nhanVien(
    taiKhoan,
    ten,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}
function renderDsnv(nvArr) {
  var contentHTML = "";
  for (var index = 0; index < nvArr.length; index++) {
    var currentNv = nvArr[index];
    var contentTr = `<tr>
        <td>${currentNv.tk}</td>
        <td>${currentNv.ten}</td>
        <td>${currentNv.email}</td>
        <td>${currentNv.ngayLam}</td>
        <td>${currentNv.chucVu}</td>
        <td>${currentNv.tinhTongLuong()}</td>
        <td>${currentNv.xepLoai()}</td>
        <td>
        <button type="button" onclick="xoaNV(${
          currentNv.tk
        })" class="btn btn-warning">Xoa</button>
        <button type="button" onclick="suaNV(${
          currentNv.tk
        })" class="btn btn-secondary">Sua</button>
        </td>
        </tr>
        `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function timKiemViTri(id, nvArr) {
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    if (item.tk == id) {
      return index;
    }
  }
  return -1;
}
function showThongTinLenFormNV(nv) {
  document.getElementById("tknv").value = nv.tk;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
function showMessageErr(idErr, message) {
  document.getElementById(idErr).style.display = "block";
  document.getElementById(idErr).innerHTML = message;
}
