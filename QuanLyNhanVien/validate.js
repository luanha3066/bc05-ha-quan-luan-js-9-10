function kiemTraRongNv(input, idErr, message) {
  if (input.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}
function kiemTraSoLuong(input, idErr, messErr) {
  var reg = /^[0-9]{3,7}$/;
  let isTK = reg.test(input);
  if (isTK) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraChu(input, idErr, messErr) {
  var reg = /^[a-zA-Z]+$/;
  let isTen = reg.test(input);
  if (isTen) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraEmail(input, idErr, messErr) {
  var reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  let isEmail = reg.test(input);
  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraMatKhau(input, idErr, messErr) {
  var reg = /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,11}$/;
  let isMatKhau = reg.test(input);
  if (isMatKhau) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraNgay(input, idErr, messErr) {
  var reg = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  let isNgayLam = reg.test(input);
  if (isNgayLam) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraLuongCB(input, idErr, messErr) {
  if (input >= 1000000 && input <= 20000000) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraChucVu(input, idErr, messErr) {
  if (input !== "Chọn chức vụ") {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
function kiemTraGioLam(input, idErr, messErr) {
  if (input >= 80 && input <= 200) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
