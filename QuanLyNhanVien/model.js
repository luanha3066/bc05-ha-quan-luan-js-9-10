function nhanVien(
  taiKhoan,
  hoTen,
  email,
  matKhau,
  ngayLam,
  luongCB,
  chucVu,
  gioLam
) {
  this.tk = taiKhoan;
  this.ten = hoTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCB = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tinhTongLuong = function () {
    var tongLuong;
    if (this.chucVu == "Sếp") tongLuong = this.luongCB * 3;
    else if (this.chucVu == "Trưởng phòng") tongLuong = this.luongCB * 2;
    else tongLuong = this.luongCB * 1;
    return tongLuong;
  };

  this.xepLoai = function () {
    var xepLoai;
    if (this.gioLam >= 192) xepLoai = "Nhan vien xuat sac";
    else if (this.gioLam >= 176) xepLoai = "Nhan vien gioi";
    else if (this.gioLam >= 160) xepLoai = "Nhan vien kha";
    else xepLoai = "Nhan vien trung binh";
    return xepLoai;
  };
}
